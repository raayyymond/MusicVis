import hypermedia.net.*;
import ddf.minim.*;
import ddf.minim.analysis.*;
import processing.serial.*;
import controlP5.*;
import java.math.*;
import java.util.*;

UDP udp;
String ip = "192.168.137.62";
int port = 7777;

Serial myPort;
int portNum = 0;

Minim minim;
FFT fft;
AudioSource in;

int bands = 100;
int barMult = 12; //bar height multiplier (visual only - doesn't affect calculations)

float hue = 0;
float bright = 0;
float hueMaxChange = 255; //smoothing values     CHANGE ME*********
float brightMult = 10; //default brightness multiplier

float hueMax = 255.0;

int w = 1024;
int h = 500;

ControlP5 cp5;

int hueSliderTicks = bands;
int brightnessTicks = 100;
Slider slider;

int leds = 100;
color[] led = new color[leds];

long lastSend = 0;
long lastShift = 0;

float delayBetweenShift = .015; //delay between each color moving down the leds (in seconds)

float targetFPS = 100;

void setup() {
  size(1154, 980);

  //setup audio lib
  minim = new Minim(this);
  in = minim.getLineIn();
  fft = new FFT(in.bufferSize(), in.sampleRate());

  //setup Serial
  //String portName = Serial.list()[portNum];
  //myPort = new Serial(this, portName, 115200);

  //setup UDP
  udp = new UDP(this, 7777);
  udp.listen(false);

  //prelim processing drawing stuff
  colorMode(HSB);
  rectMode(CORNERS);
  noStroke();

  //ControlP5 library setup
  cp5 = new ControlP5(this);

  //frequency sliders
  cp5.addSlider("Min Freq")
    .setPosition(0, h)
    .setSize(w, 40)
    .setRange(0, 513)
    .setValue(0)
    .setSliderMode(Slider.FLEXIBLE);
  cp5.addSlider("Max Freq")
    .setPosition(0, h + 40)
    .setSize(w, 40)
    .setRange(0, 513)
    .setValue(bands)
    .setSliderMode(Slider.FLEXIBLE);

  //brightness slider
  cp5.addSlider("Brightness")
    .setPosition(w + 50, 0)
    .setSize(50, height)
    .setRange(0, 100)
    .setValue(brightMult)
    .setSliderMode(Slider.FLEXIBLE);
}

void draw() {
  background(20);
  textSize(18);

  fft.forward(in.mix);

  int fullSum = 0;
  
  float[] notes = new float[12];

  //draw top bands
  for (int i = 0; i < fft.specSize(); i++) {
    float len = (float) w / fft.specSize();
    float val = fft.getBand(i);
    int noteNum = (int) ((12 * (Math.log(fft.indexToFreq(i) / 440.0) / Math.log(2)) + 69.0) % 12);
    if (noteNum >= 0)
      notes[noteNum] += val;
    else
      notes[-noteNum] += val;

    fill(hueMax / fft.specSize() * i, 255, 255);
    rect(i * len, h, (i * len) + len, h - (val * barMult));
    
    fullSum += val;
  }
  
  int sum = 0;

  //draw bottom bands, calculations are based off here
  int min = (int) cp5.getController("Min Freq").getValue();
  int max = (int) cp5.getController("Max Freq").getValue();

  for (int i = 0; i < 12; i++) {
    float len = (float) w / (12);
    float val = notes[i];

    fill(hueMax / 12 * i, 255, 255);
    rect((i - min) * len, height, (i - min) * len + len, max(height - (val), height - 400));

    sum += val;
  }
  
  int maxNote = 0;
  float maxVal = notes[0];
  for(int i = 1; i < 12; i++) {
    if(notes[i] > maxVal) {
      maxNote = i;
      maxVal = notes[i];
    }
  }

  float hue = hueMax / 12 * maxNote;
  float bright = min(fullSum * cp5.getController("Brightness").getValue(), 255);

  //draw square
  color col = color((int) hue, 255, bright);
  fill(col);
  rect(w / 2 - 25, 0, w / 2 + 25, 50);

  //Serial stuff
  /*String msg = "";
   if(bright > 20) {
   int red = col >> 16 & 0xFF;
   int green = col >> 8 & 0xFF;
   int blue = col & 0xFF;
   msg = red + "," + green + "," + blue + "," + ((int) (bright / 255.0));
   } else {
   msg = "0,0,0";
   }
   myPort.write(msg);*/

  //animate LEDs
  long current = millis();
  if (current - lastShift >= delayBetweenShift * 1000) {
    if (bright > 80) {
      led[leds / 2] = col;
    } else {
      led[leds / 2] = color(0, 0, 0);
    }
    
    color[] oldLed = Arrays.copyOf(led, leds);
    for (int i = 0; i < leds / 2; i++) {
      led[i] = oldLed[i + 1];
    }
    for (int i = leds - 1; i >= leds / 2; i--) {
      led[i] = oldLed[i - 1];
    }

    lastShift = current;
  }
  
  //update LEDs
  current = millis();
  if (current - lastSend >= 1000 / targetFPS) {
    String msg = "";
    for (int i = 0; i < leds; i++) {
      char index = (char) i;
      char red = (char) (led[i] >> 16 & 0xFF);
      char green = (char) (led[i] >> 8 & 0xFF);
      char blue = (char) (led[i] & 0xFF);
      msg += "" + index + red + green + blue;
    }
    udp.send(msg, ip, port);

    print(round(1000.0 / (current - lastSend)) + " FPS\n");
    lastSend = current;
  }
}
